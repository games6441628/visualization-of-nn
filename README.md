## Visualization of Neural Networks


Description: This is an application for semestral work. Neural networks are nowadays becoming more and more popular. Many people are trying to understand how they work. However, it is often hard to actually understand and verify them. Visualization can help with that. Great visualization can provide the user with a better understanding of how neural networks work, to let the user see what is covered beneath this black box. We choose to visualize the 10-dimensional visual representations of images from the MNIST data set obtained by our trained simple neural network.

<img src="./nnViz.PNG" width="400">

Authors: Yanina Arameleva & Jan Cuhel

