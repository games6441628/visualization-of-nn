// init d3 Window
var width = 700;
var height = 705;
var count = 100;
var svg = d3.select("#viz").append("svg")
.attr("width", width)
.attr("height", height)
.attr("viewBox", [0, 0, width, height])
.style("border", "1px solid black")
.on("click", deselected);
var g = svg.append("g");

// will change latter
var myColors = {
    0: "#F3EA68",
    1: "#F57914",
    2: "#FF99A0",
    3: "#C92CAC" ,
    4: "#41247c" ,
    5: "#B99BE9",
    6: "#1759B5",
    7: "#33BDEB",
    8: "#0e6e51",
    9:  "gray",
  };
var WRONGLY_PREDICTED = true;
var CORRECTLY_PREDICTED = true;
var COLOR_BY_LABELS = true;
var checkboxNumberToShow =  Array(10).fill(1);
// amount of circles for each type of label
var USE_TRAIN_DATA = true;
var USE_TEST_DATA = false;
var USE = USE_TRAIN_DATA;
var reductionType = "pca"
var circleAmount = 1000;
// console.log(testData2D);
var data = [];
loadData("pca", 500, 3)
circles = mapData(data);
initWindow();


function loadData(type, scale, size){
    // x, y, r, c, img
    // console.log("WRONGLY_PREDICTED: " + WRONGLY_PREDICTED)
    // console.log("CORRECTLY_PREDICTED: " + CORRECTLY_PREDICTED)
    var checker = Array(10).fill(0)
    var stopAmount = 0;
    data = []
    var index = 0
    var showData = []
    if(USE == USE_TRAIN_DATA){
        showData = trainData2D;
    }else{
        showData = testData2D;
    }
    for(i = 0; i < showData.labels.length; i++ && stopAmount <= circleAmount*10) {
        if(checker[showData.labels[i]] < circleAmount &&
            ((checkboxNumberToShow[showData.labels[i]] && COLOR_BY_LABELS) ||
            (checkboxNumberToShow[showData.predicted_numbers[i]] && !COLOR_BY_LABELS)) &&
            ((WRONGLY_PREDICTED && showData.predicted_numbers[i] != showData.labels[i]) ||
            (CORRECTLY_PREDICTED && showData.predicted_numbers[i] == showData.labels[i]))){
                // choose color
                nodeColor = COLOR_BY_LABELS ? showData.labels[i] : showData.predicted_numbers[i];
                data[index] = { x: showData[type][i][0]*scale, y:  showData[type][i][1]*scale, r: size,
                c: nodeColor, label: showData.labels[i], ind: i, predicted: showData.predicted_numbers[i]}
            checker[showData.labels[i]] += 1;
            stopAmount++;
            index++;
        }
    }
}

function mapData(data){
    var circles = g.selectAll("circle")
    .data(data)
    .enter().append("circle")
    .attr("id", (d, i) => "circle" + i)
    .attr("cx", d => d.x)
    .attr("cy", d => d.y)
    .attr("r", d => d.r)
    .attr("ind", d => d.ind)
    .attr("predicted", d => d.predicted)
    .attr("label", d => d.label)
    .attr("fill", d => myColors[d.c])
    .attr("stroke", d => {return d.predicted === d.label ? "black" : "red"})
    .attr("stroke-width", d => {return d.predicted === d.label ? 1 : 1})
    .on("click", selected); //Here we are adding selection
    return circles
}
//checkboxes for numbers
const numberCheckbox = document.getElementById("numberCheckbox");
for(var i = 0; i < 10; i++){
    var checkbox = document.createElement('input');
    var myDiv = document.createElement('div');
    var dot = document.createElement('dot');
    myDiv.classList.add('checkBoxes')
    dot.classList.add('dot')
    dot.style.backgroundColor = myColors[i]
    checkbox.type = "checkbox";
    checkbox.name = "name";
    checkbox.checked = true;
    checkbox.id = i;
    var label = document.createElement('label');
    label.classList.add('labels')
    label.classList.add('numLabels')

    label.htmlFor = i;

    checkbox.addEventListener('change', function(event, d) {
        checkboxNumberToShow[this.id] = (checkboxNumberToShow[this.id] + 1) % 2
        // console.log(checkboxNumberToShow)
        reloadAll(reductionType, false);
    });

    label.appendChild(document.createTextNode(i + ":   "));
    myDiv.appendChild(dot);

    myDiv.appendChild(label);
    myDiv.appendChild(checkbox);

    numberCheckbox.appendChild(myDiv);

}
// radioButtons
const  labelsRad = document.getElementById("labelsRad");
const predictionRad = document.getElementById("predictionRad");
labelsRad.addEventListener('change', function(event, d) {
    COLOR_BY_LABELS = true;
    reloadAll(reductionType, false);

});
predictionRad.addEventListener('change', function(event, d) {
    COLOR_BY_LABELS = false;
    reloadAll(reductionType, false);

});

//checkboxes for predictions
const  predictedCheckbox = document.getElementById("predictedCheckbox");
for(var i = 0; i < 2; i++){
    var checkbox = document.createElement('input');
    var myDiv = document.createElement('div');
    myDiv.classList.add('checkBoxes')
    // Assigning the attributes
    // to created checkbox
    checkbox.type = "checkbox";
    checkbox.name = "name";
    checkbox.checked = true;
    checkbox.id = i;
    //checkbox.classList.add('checkBoxes')
    var label = document.createElement('label');
    label.classList.add('labels')

    label.htmlFor = i;

    checkbox.addEventListener('change', function(event, d) {
        if(this.id == 0){
            WRONGLY_PREDICTED = (WRONGLY_PREDICTED + 1) % 2
        }else{
            CORRECTLY_PREDICTED = (CORRECTLY_PREDICTED + 1) % 2
        }
        reloadAll(reductionType, false);
    });
    if(i == 0){
        label.appendChild(document.createTextNode("Wrongly:"));

    }else{
        label.appendChild(document.createTextNode("Correctly:"));
    }
    myDiv.appendChild(label);
    myDiv.appendChild(checkbox);

    predictedCheckbox.appendChild(myDiv);

}

// data types
const trainButt = document.getElementById("trainButt");
const testButt = document.getElementById("testButt");
const dataType = document.getElementById("dataType");

//reduction
const pcaButt = document.getElementById("pcaButt");
const tsneButt = document.getElementById("tsneButt");
const umapButt = document.getElementById("umapButt");
const redType = document.getElementById("redType");
// amount of nodes
const amountInput = document.getElementById("amountInput");
const amountTxt = document.getElementById("amountTxt");
// imgs
const nodeImg = document.getElementById("nodeImg");
const nodeTxt = document.getElementById("nodeTxt");
const nodeDetails = document.getElementById("nodeDetails");

amountInput.addEventListener('change', function () {
    amountTxt.innerHTML = amountInput.value;
    circleAmount = amountInput.value;
    // console.log("act")
    reloadAll(reductionType, false);

  }, false);

pcaButt.addEventListener("click", function() {
    reloadAll("pca", true);
    redType.innerHTML = "Reduction: PCA (500x)"

});
umapButt.addEventListener("click", function() {

    reloadAll("umap", true);
    redType.innerHTML = "Reduction: UMAP (100x)"

});
tsneButt.addEventListener("click", function() {

    reloadAll("tsne", true);
    redType.innerHTML = "Reduction: TSNE (10x)"

});

trainButt.addEventListener("click", function() {
    USE = USE_TRAIN_DATA;
    dataType.innerHTML="Data type: Train Data";
    amountInput.max = 6000
    reloadAll(reductionType, true);
});

testButt.addEventListener("click", function() {
    USE = USE_TEST_DATA;
    dataType.innerHTML="Data type: Test Data";
    amountInput.max = 1000
    amountTxt.innerHTML = 1000
    amountInput.value = 1000
    circleAmount = 1000
    reloadAll(reductionType, true);
});

function reloadAll(reducType, shouldInitWindow){
    reductionType = reducType
    // change data
    LoadDataWithType(reducType);
    // remove old data
    d3.selectAll("circle").remove()
    // map new data
    circles = mapData(data)
    // rescale window
    if(shouldInitWindow){
        initWindow();
    }
}

function LoadDataWithType(type){
    if(type === "pca"){
        loadData("pca", 500, 3)
    } else if(type === "tsne"){
        loadData("tsne", 10, 4);
    }else if(type === "umap"){
        loadData("umap", 100, 10)
    }
}


function initWindow(){
    var minX;
    var maxX;
    var minY;
    var maxY;
    var first = true;
    for(circle of g.selectAll("circle")) {
        var cx = Number(circle.getAttribute("cx"));
        var cy = Number(circle.getAttribute("cy"));
        var r = Number(circle.getAttribute("r"));
        if(first) {
            first = false;
            minX = cx - r;
            maxX = cx + r;
            minY = cy - r;
            maxY = cy + r;
        }
        else {
            minX = Math.min(minX, cx - r);
            maxX = Math.max(maxX, cx + r);
            minY = Math.min(minY, cy - r);
            maxY = Math.max(maxY, cy + r);
        }
    }
    var sceneWidth = maxX - minX;
    var sceneHeight = maxY - minY;
    var scale = Math.min(width/sceneWidth, height/sceneHeight);
    // console.log("x: (" + Math.round(maxX) + ", " +  Math.round(minX) + "), y:( "
    // + Math.round(maxY) + ", " + Math.round(minY)+")");
    var zoomExtent = d3.zoom().scaleExtent([0.1, 10]).on("zoom", zoomed);
    svg.call(zoomExtent);
    svg.call(zoomExtent.transform, d3.zoomIdentity.scale(scale).translate(-minX, -minY));

}

let chart = null;

//Calculate and set initial transformation matrix

function deselected(event, d) {
    // d3.selectAll("circle").attr("stroke", "black");
    d3.selectAll("circle").attr("fill", d=>myColors[d.c]);
    event.stopPropagation();
   // nodeDetails.class = "card invisible"
    //console.log("disselected")
    nodeDetails.classList.add('invisible')

    if (chart != null) {
        chart.destroy();
        chart = null
    }
}
// console.log(trainData2D)
function selected(event, d) {
    deselected(event, d);

    // event.srcElement.setAttribute("stroke", "red");
    event.srcElement.setAttribute("fill", "#0EE200");
    nodeDetails.classList.remove('invisible')
    //console.log("selected")
    if(USE_TRAIN_DATA == USE){
        nodeImg.src = "./imgs/train_images/" + d.ind + ".png";
    }else{
        nodeImg.src = "./imgs/test_images/" + d.ind + ".png";
    }
    if(d.predicted === d.label){
        nodeTxt.innerHTML = "Number: <b>" +  d.label + "</b><br/><b>Predicted correctly</b>"+
        "<br/>Clicked at: <br/>x: <b>" + Math.round((d.x + Number.EPSILON)*100)/100 + "</b>, y: <b>" + Math.round((d.y + Number.EPSILON)*100)/100 + "</b><br/>";
    }else{
        nodeTxt.innerHTML = "Number: <b>" +  d.label + "</b><br/><b>Predicted incorrectly</b>" +
        "<br/>Predicted: <b>" + d.predicted +
        "</b><br/>Clicked at: <br/>x: <b>" + Math.round((d.x + Number.EPSILON)*100)/100 + "</b>, y: <b>" + Math.round((d.y + Number.EPSILON)*100)/100 + "</b><br/>";
    }
    // Show prediction likelihoods
    // if(USE_TRAIN_DATA == USE){
    //     console.log(trainData2D.prediction[d.ind].map(x => Math.round(100*x)))
    // } else {
    //     console.log(testData2D.prediction[d.ind].map(x => Math.round(100*x)))
    // }

    // https://apexcharts.com/javascript-chart-demos/bar-charts/custom-datalabels/
    var options = {
        series: [{
        data: (USE_TRAIN_DATA == USE ? trainData2D.prediction[d.ind] : testData2D.prediction[d.ind]).map(x => Math.round((x + Number.EPSILON)*10000)/100)
      }],
        chart: {
        type: 'bar',
        height: 210
      },
      plotOptions: {
        bar: {
          borderRadius: 2,
          horizontal: true,
          distributed: true,
        }
      },
      dataLabels: {
        enabled: false
      },
      colors: ["#F3EA68", "#F57914","#FF99A0", "#C92CAC", "#41247c", "#B99BE9", "#1759B5", "#33BDEB", "#0e6e51", "gray",],
      xaxis: {
        categories: ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],
        title: {
            text: 'Likelihood [%]'
        }
      },
      title: {
        text: 'Predictions',
        align: 'center',
        floating: true
      },
      tooltip: {
        theme: 'dark',
        x: {
          show: false
        },
        y: {
          title: {
            formatter: function () {
              return '%'
            }
          }
        }
      },
      legend: {
        show: false
      }
      };
      chart = new ApexCharts(document.querySelector("#chartContainer"), options);
      chart.render();
}

function zoomed({transform}) {
    g.attr("transform", transform);
}

// const element = document.getElementById("testBut");
// element.addEventListener("click", function() {
//   for(i = 0; i < 900; i++) {

//     var rot = 0.7071
//     var cos15 = 0.96592
//     var sin15 = 0.258819
//     var xx = data[i].x
//     var yy = data[i].y
//     data[i] = { x: xx*cos15-yy*sin15, y:  xx*sin15+yy*cos15, r: data[i].r , c: data[i].c }
//  }
//     d3.selectAll("circle").remove()
//     circles = mapData(data)
// });
